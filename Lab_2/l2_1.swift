var carsStructure: [String: String] = ["ABC-987": "John Smith", "SLOW-0.0": "Franck Savary", "FAST-99": "Raul Ferrain", "KOA-569":"Elizabeth The Queen"];

/** Printing all the driver names  **/
print("Driver Names: ")
let driversNames = [String](carsStructure.values)

for nameDriver in driversNames {
	print("\(nameDriver)")
}
print("\n\r")


/** Printing register ids for cars **/
print("Register Ids:")
let registerIds = [String](carsStructure.keys)

for idCar in registerIds {
	print("\(idCar)");
}
print("\n\r")


/** Printing register ids in ascending order **/
print("Register Ids in Ascending Order:")
let registerIdsAsc = registerIds.sorted()

for idCar in registerIdsAsc {
	print("\(idCar)");
}
print("\n\r")