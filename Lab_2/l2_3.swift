class Vehicle {
	
	fileprivate var speed = 0.0
	let registerId: String

	init(registerId: String) {
		self.registerId = registerId
	}

	convenience init() {
		self.init(registerId: "unknown")
	}
}

class Car: Vehicle {
	
	private let acceleration: Double
	fileprivate var topSpeed: Double?
	
	init(registerId: String, acceleration: Double) {
		self.acceleration = acceleration
		super.init(registerId: registerId)
	}
	
	func speed() -> Double {
		return self.speed
	}
	
}


/** Initializing Cars **/
var c1 = Car(registerId: "ABC-987", acceleration: 10.0)
var c2 = Car(registerId: "SLOW-0.0", acceleration: 2.0)
var c3 = Car(registerId: "FAST-99", acceleration: 18.0)
var c4 = Car(registerId: "KOA-569", acceleration: 5.0)

var carsStructure: [String: Car] = ["John Smith": c1, "Franck Savary": c2, "Raul Ferrain": c3, "Elizabeth The Queen": c4]


/** Printing all the drivers **/
print("Drivers:")
let drivers = [String](carsStructure.keys)

for driverName in drivers {
	print("\(driverName)")
}
print("\n\r")


/** Printing register ids for all cars **/
print("Register Ids:")
let cars = [Car](carsStructure.values)

for carCurr in cars {
	print("\(carCurr.registerId)")
}
print("\n\r")


/** Printing register ids for all cars in ascending order **/
print("Register Ids in Ascending Order")
var registerIdsAsc = [String]()

for carCurr in cars {
	registerIdsAsc.append(carCurr.registerId)
}

registerIdsAsc.sort()
for id in registerIdsAsc {
	print("\(id)")
}