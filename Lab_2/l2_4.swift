var carsStructure = [String: Set<String>]()

var set1: Set<String> = ["ABC-987","SLOW-0.0","RIT-456"]
var set2: Set<String> = ["KOA-569"]
var set3 = Set<String>()
var set4: Set<String> = ["FAST-99","ABC-988","VCB-672"]
			
carsStructure["John Smith"] = set1
carsStructure["Franck Savary"] = set2	
carsStructure["Raul Ferrain"] = set3
carsStructure["Elizabeth The Queen"] = set4

/** Printing all drivers names with at least one car **/
print("Driver Names with at least One Car:")
for (driver, cars) in carsStructure {
	if cars.count >= 1 {
		print("\(driver)")
	}
}
print("\n\r")


/** Printing Register Ids for all cars in ascending order **/
print("Register Ids in Ascending Order:")
var cars = [String]()
let sets = [Set<String>](carsStructure.values)

for s in sets {
	for car in s {
		cars.append(car)
		cars.sort()
	}
}

for c in cars {
	print("\(c)")
}
