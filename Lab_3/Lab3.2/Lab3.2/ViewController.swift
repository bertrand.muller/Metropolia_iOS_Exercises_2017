//
//  ViewController.swift
//  Lab3.2
//
//  Created by iosdev on 20.1.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var sliderR: UISlider!
    @IBOutlet weak var sliderG: UISlider!
    @IBOutlet weak var sliderB: UISlider!
    
    private var r: CGFloat!
    private var g: CGFloat!
    private var b: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        r = CGFloat(sliderR.value / 255)
        g = CGFloat(sliderG.value / 255)
        b = CGFloat(sliderB.value / 255)
        updateColorBackgound()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateColorBackgound() {
        self.view.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: 1.0)
    }
    
    @IBAction func sliderRChanged(_ sender: Any) {
        r = CGFloat(sliderR.value / 255)
        updateColorBackgound()
    }
    
    @IBAction func sliderGChanged(_ sender: Any) {
        g = CGFloat(sliderG.value / 255)
        updateColorBackgound()
    }
    
    @IBAction func sliderBChanged(_ sender: Any) {
        b = CGFloat(sliderB.value / 255)
        updateColorBackgound()
    }
    
}

