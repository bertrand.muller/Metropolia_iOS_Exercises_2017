//
//  ViewController.swift
//  Lab3.1
//
//  Created by iosdev on 20.1.2017.
//  Copyright © 2017 Bertrand Müller. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var myLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        myLabel.text = "What?"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func buttonPressed(_ sender: Any) {
        print("Button is pressed!")
        myLabel.text = "Hello World"
    }

}

