// https://swiftlang.ng.bluemix.net/#/repl/587cfb17a04e046c4b72cbd8

class Counter {
    
    private let minimum: Int
    private let maximum: Int
    private let originalValue: Int
    private var count: Int
    
    init(min: Int, max: Int, originalVal: Int) {
        self.minimum = min
        self.maximum = max
        self.originalValue = originalVal
        self.count = originalVal
    }
    
    convenience init(mi: Int, ma: Int) {
        self.init(min: mi, max: ma, originalVal: mi)
    }
    
    func increment() {
        if((self.count + 1) <= self.maximum) {
            self.count += 1
        }
    }
    
    func decrement() {
        if((self.count - 1) >= self.minimum) {
            self.count -= 1
        }
    }
    
    func reset() {
        self.count = self.originalValue
    }
    
    func getValue() -> Int {
        return self.count
    }
    
}

/*** TESTS ***/
var counter1 = Counter(min: 0, max: 10, originalVal: 5)
var counter2 = Counter(mi: 8, ma: 12)


/*** TEST COUNTER 1 ***/
print("Original value Counter 1: \(counter1.getValue())")

counter1.increment()
counter1.increment()
print("Increment twice Counter 1: \(counter1.getValue())")

counter1.decrement()
print("Decrement once Counter 1: \(counter1.getValue())")

counter1.reset();
print("Reset Counter 1: \(counter1.getValue())\n\r")


/*** TEST COUNTER 2 ***/
print("Original value Counter 2 (Min): \(counter2.getValue())")

counter2.decrement()
counter2.decrement()
counter2.decrement()
print("Decrement three times Counter 2: \(counter2.getValue())")

counter2.increment()
counter2.increment()
counter2.increment()
counter2.increment()
counter2.increment()
print("Increment five times Counter 2: \(counter2.getValue())")

counter2.reset();
print("Reset Counter 2: \(counter2.getValue())")