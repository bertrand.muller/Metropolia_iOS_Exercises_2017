class Human {
    
    private var name: String
    private var height: Double?
    private var weight: Double?
    
    init(nameHuman: String) {
        self.name = nameHuman
    }
    
    func getName() -> String {
        return self.name
    }
    
    func getHeight() -> Double {
    
        if let h = self.height { 
            return h
        } 
        
        return 0
    }
    
    func setHeight(h: Double) {
        if h > 0.10, h <= 2.30 {
            self.height = h
        } else {
            print("Value for height not valid!")
        }
    }
    
    func getWeight() -> Double {
    
        if let w = self.weight { 
            return w
        }
        
        return 0
    }
    
    func setWeight(w: Double) {
        if w > 2, w <= 200 {
            self.weight = w
        } else {
            print("Value for weight not valid!")
        }
    }
    
    func calculateBMI() -> Double {
    
        if let h = self.height, let w = self.weight {
            let squareHeight = h*h
            let bmi = w / squareHeight
            return bmi
        }
        
        return 0
        
    }
    
}

/*** TESTS ***/
var human1 = Human(nameHuman: "John")
var human2 = Human(nameHuman: "Franck")

/*** TEST HUMAN 1 ***/
human1.setHeight(h: 1.90)
human1.setWeight(w: 64)

print("Height Human 1: \(human1.getHeight())")
print("Weight Human 1: \(human1.getWeight())")
print("BMI Human 1: \(human1.calculateBMI())\n\r")

/*** TEST HUMAN 2 ***/
human2.setHeight(h: 1.62)
human2.setWeight(w: 78)

print("Height Human 2: \(human2.getHeight())")
print("Weight Human 2: \(human2.getWeight())")
print("BMI Human 2: \(human2.calculateBMI())")